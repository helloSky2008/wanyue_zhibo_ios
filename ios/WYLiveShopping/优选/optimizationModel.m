// +----------------------------------------------------------------------
// |万岳科技开源系统 [山东万岳信息科技有限公司]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2022 https://www.sdwanyue.com All rights reserved.
// +----------------------------------------------------------------------
// | 万岳科技相关开源系统，需标注"代码来源于万岳科技开源项目"后即可免费自用运营，前端运营时展示的内容不得使用万岳科技相关信息
// +----------------------------------------------------------------------
// | Author: 万岳科技开源官方 < wanyuekj2020@163.com >
// +----------------------------------------------------------------------

#import "optimizationModel.h"

@implementation optimizationModel
-(instancetype)initWithDic:(NSDictionary *)dic{
    self = [super init];
    if (self) {
        self.thumb = minstr([dic valueForKey:@"image"]);
        self.name = minstr([dic valueForKey:@"store_name"]);
        self.goodsID = minstr([dic valueForKey:@"id"]);
        self.price = minstr([dic valueForKey:@"price"]);
        self.cate_id = minstr([dic valueForKey:@"cate_id"]);
        self.sales = minstr([dic valueForKey:@"sales"]);
        self.unit_name = minstr([dic valueForKey:@"unit_name"]);
        self.vip_price = minstr([dic valueForKey:@"vip_price"]);
        self.activity = [dic valueForKey:@"activity"];
    }
    return self;
}

@end
